FROM python:slim

WORKDIR /tests/

COPY requirements.txt .
COPY requirements-dev.txt .

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir -r requirements-dev.txt

COPY . .

ARG COMMIT
ENV CI_COMMIT_BRANCH=${COMMIT}

RUN pip install .

ENTRYPOINT [ "{{ project.name }}" ]