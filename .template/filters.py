def superior(package, version):
    version = str(version).split('.')
    if len(version) > 2:
        return '>='.join(package, '.'.join(version[:2]))
    return '>='.join((package, '.'.join(version)))

def equal(package, version):
    return '=='.join((package, str(version)))

def get_filters(context):
    return {
        'superior': superior,
        'equal': equal
    }