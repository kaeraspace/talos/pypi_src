import pytest
import sys

@pytest.fixture
def captured_stdout(monkeypatch):
    stdout_data = {'lines': [], 'nb_calls': 0}

    def mocked_write(str_):
        stdout_data['lines'].append(str_)
        stdout_data['write_calls'] += 1

    monkeypatch.setattr(sys.stdout, 'write', mocked_write)
    return stdout_data

@pytest.fixture(scope='session')
def counter():
    class Counter:
        def __init__(self):
            self.cnt = 0

        def incr(self, nb=1):
            self.cnt += nb

        @property
        def val(self):
            return self.cnt

    yield Counter()
