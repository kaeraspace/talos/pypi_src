import pytest
import {{ project.name | lower }}
from {{ project.name | lower }} import hello_world

def test_default_test_structure(monkeypatch):
    # Given
    myinput = 2

    # When
    result = myinput * 2

    # Then
    assert result == 4

def test_hello_world(captured_stdout, counter):
    # Given

    # When
    hello_world()
    counter.incr()

    # Then
    assert captured_stdout['lines'][0] == 'Hello, world!'
    assert counter.val == 1

def test_mocked_hello_world(monkeypatch, captured_stdout):
    # Given
    new_hello_world = 'Hello world...'

    # When
    with monkeypatch.context() as m:
        m.setattr({{ project.name | lower }}, 'hello_world', lambda: new_hello_world)
        {{ project.name | lower }}.hello_world()

    # Then
    assert captured_stdout['lines'][0] == new_hello_world

def test_counter(counter):
    # Given
    nb = 3

    # When
    counter.incr(nb)

    # Then
    assert counter.val == 4

@pytest.mark.parametrize('input_,expected', [
    (1, 2), (2, 4), (3, 6),
    (4, 8), (0, 0), (7, 14)
])
def test_mul_by_2(input_, expected):
    assert input_*2 == expected

@pytest.mark.skip(reason='this is how to skip a test')
def test_skipped():
    assert False

@pytest.mark.xfail
def test_expected_to_fail():
    assert False

def test_raise_error():
    with pytest.raises(ZeroDivisionError):
        1/0
