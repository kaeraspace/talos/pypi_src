# {{ project.name }}

{{ project.desc }}

# Testing

Install package using: `pip install -e .`

Then run `pytest`

# Installing

Install using pip

> pip intall {{ project.name }}

# Contributors

{% for contributor in contributors %}
* {{ contributor }}
{% endfor %}
